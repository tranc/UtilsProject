package com.my.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utils implementation for using in the feature.
 * 
 * @author tranc, C1 SetCon
 * @since R11-0
 */
public class MyUtils {
    //private static final Logger LOG = LogManager.getLogger(MyUtils.class);


    public static void main(String[] args) throws InterruptedException {
        MyUtils myUtils = new MyUtils();
        
        String path = "src/main/resources/frontendOrderValidation.drl";
        //String regex = "RESULT.addMessage\\((.*?),";
        String regex = "MessageCodeEnum.(.*?),";
        
        List<String> distinctMatches = myUtils.getDistinctMatchesStringWithRegex(path, regex);
        
        String path1 = "src/main/resources/frontendOrderValidationAnalysist.txt";
        List<String> distinctMatches1 = myUtils.getDistinctMatchesStringWithRegex(path1, regex);
        
        
        String path2 = "src/main/resources/frontendOrderValidationIFS.txt";
        List<String> distinctMatches2 = myUtils.getDistinctMatchesStringWithRegex(path2, regex);
        
        System.out.println("Total matches in drools: " + distinctMatches.size());
        System.out.println("Total matches of analyst: " + distinctMatches1.size());
        System.out.println("Total matches from ifs: " + distinctMatches2.size());
        
        List<String> notExistInIFS = new ArrayList<String>();
        
        for (String matches : distinctMatches) {
            if (!distinctMatches2.contains(matches)) {
                notExistInIFS.add(matches);
                System.out.println(matches);
            }
        }
        
        /*if (distinctMatches.containsAll(distinctMatches1) && distinctMatches1.containsAll(distinctMatches)) {
            System.out.println("same contents");
        } else {
            System.out.println("content not the same");
        }
        /*for (String matches : distinctMatches) {
            System.out.println(matches);
        }
        System.out.println("Total matches: " + distinctMatches.size());*/
    }
    
    /**
     * 
     * @param path
     * @param regex
     * @return
     */
    public List<String> getDistinctMatchesStringWithRegex(String path, String regex) {
        List<String> allMatches = getMatchesStringWithRegex(path, regex);
        List<String> distinctMatches = new ArrayList<String>();
        
        for (String matches : allMatches) {
            if (!distinctMatches.contains(matches)) {
                distinctMatches.add(matches);
            }
        }
        
        return distinctMatches;
    }
    
    /**
     * get a list of matches String from a file with a given regex.
     * 
     * @param path
     * @param regex
     */
    public List<String> getMatchesStringWithRegex(String path, String regex) {
        //LOG.info("getMatchesStringWithRegex" );
        List<String> allMatches = new ArrayList<String>();

            try {
                InputStream inputStream = new FileInputStream(path);;
                InputStreamReader inputReader = new InputStreamReader(inputStream);
                
                //Reader stream = resource.getReader();
                BufferedReader reader = new BufferedReader(inputReader);
                StringBuffer buf = new StringBuffer();    
                
                String str = "";
                while ((str = reader.readLine()) != null) {    
                    buf.append(str + "\n" );
                }   
                reader.close();
               
                String text = buf.toString();

                //Pattern p = Pattern.compile("^[a-zA-Z]+([0-9]+).*");
                //Pattern pattern = Pattern.compile("RESULT.addMessage\\((.*?),");
                Pattern pattern = Pattern.compile(regex);
                Matcher m = pattern.matcher(text);
    
                
                while (m.find()) {
                    allMatches.add(m.group(1));
                }
                
            } catch (IOException e) {
                e.printStackTrace();
            }
        return allMatches;
    }
}
